Get started with woolmark
-----------------------------------
Welcome to your new Java Web Starter! 
               
Get started with this Java Web Starter application that is using IBM Data Cache Java Native APIs, powered by WebSphere eXtreme Scale technology.

1. [Install the cf command-line tool](https://www.ng.bluemix.net/docs/redirect.jsp?name=cf-instructions).
2. [Download the starter application package](https://ace.ng.bluemix.net:443/rest/../rest/apps/fd175918-a095-4968-8248-8fedea706f31/starter-download).
3. Extract the package and cd to it.
4. Connect to BlueMix:

		cf api https://api.ng.bluemix.net

5. Log into BlueMix:

		cf login -u runne@siren.ocn.ne.jp
		cf target -o runne@siren.ocn.ne.jp -s dev
		
6. Compile the JAVA code and generate the war package using ant.
7. Deploy your app:

		cf push woolmark -p woolmark.war


8. Access your app: [http://woolmark.ng.bluemix.net](http://woolmark.ng.bluemix.net)